/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  Dimensions,
} from 'react-native';

import { createImageProgress } from 'react-native-image-progress';
import FastImage from 'react-native-fast-image';
import Progress from 'react-native-progress/CircleSnail';

// const Image = createImageProgress(FastImage);
var screen = Dimensions.get('window');
export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image 
        style={{
          width: 320,
          height: 240,
        }}
        source={{ uri: 'http://www.savethecat.com/wp-content/uploads/2015/06/cats.jpg' }}/>
        {/* <Image
          source={{ uri: 'http://www.savethecat.com/wp-content/uploads/2015/06/cats.jpg' }}
          indicator={Progress}
          indicatorProps={{
            size: 80,
            borderWidth: 0,
            color: 'rgba(150, 150, 150, 1)',
            unfilledColor: 'rgba(200, 200, 200, 0.2)'
          }}
          onLoadStart={e => console.log('Loading Start')}
          onProgress={e =>
            console.log(
              'Loading Progress ' +
              e.nativeEvent.loaded / e.nativeEvent.total
            )
          }
          style={{
            width: 320,
            height: 240,
          }} /> */}
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})